package com.TestAPI.jobPortal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.TestAPI.jobPortal.model.PostJob;
import com.TestAPI.jobPortal.repository.PostJobRepository;

@RestController
public class PostJobController {
	@Autowired
	  private final PostJobRepository repository;
		PostJobController(PostJobRepository repository) {
	    this.repository = repository;
	  }

	  @GetMapping("/postJobs")
	  List<PostJob> all() {
	    return repository.findAll();
	  }
	

	  @PostMapping("/postJobs")
	  PostJob newPostJob(@RequestBody PostJob newPostJob) {
	    return repository.save(newPostJob);
	  }
	


	  @DeleteMapping("/postJobs/{id}")
	  void deletePostJob(@PathVariable Long id) {
	    repository.deleteById(id);
	  }
}
