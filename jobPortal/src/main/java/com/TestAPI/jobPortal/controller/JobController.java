package com.TestAPI.jobPortal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.TestAPI.jobPortal.model.Job;
import com.TestAPI.jobPortal.repository.JobRepository;

@RestController
public class JobController {
	@Autowired
	  private final JobRepository repository;
	JobController(JobRepository repository) {
	    this.repository = repository;
	  }

	  @GetMapping("/jobs")
	  List<Job> all() {
	    return repository.findAll();
	  }
	

	  @PostMapping("/jobs")
	  Job newJob(@RequestBody Job newJob) {
	    return repository.save(newJob);
	  }
	


	  @DeleteMapping("/jobs/{id}")
	  void deleteJob(@PathVariable Long id) {
	    repository.deleteById(id);
	  }
}
