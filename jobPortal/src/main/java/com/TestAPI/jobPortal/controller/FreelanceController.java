package com.TestAPI.jobPortal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.TestAPI.jobPortal.model.Freelance;
import com.TestAPI.jobPortal.repository.FreelanceRepository;

@RestController
public class FreelanceController {
	@Autowired
	  private final FreelanceRepository repository;
		FreelanceController(FreelanceRepository repository) {
	    this.repository = repository;
	  }

	  @GetMapping("/freelances")
	  List<Freelance> all() {
	    return repository.findAll();
	  }
	

	  @PostMapping("/freelances")
	  Freelance newFreelance(@RequestBody Freelance newFreelance) {
	    return repository.save(newFreelance);
	  }
	


	  @DeleteMapping("/freelances/{id}")
	  void deleteFreelance(@PathVariable Long id) {
	    repository.deleteById(id);
	  }
}
