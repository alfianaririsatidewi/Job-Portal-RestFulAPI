package com.TestAPI.jobPortal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.TestAPI.jobPortal.model.PostApplication;
import com.TestAPI.jobPortal.repository.PostApplicationRepository;


@RestController
public class PostApplicationController {
	@Autowired
	  private final PostApplicationRepository repository;
	PostApplicationController(PostApplicationRepository repository) {
	    this.repository = repository;
	  }

	  @GetMapping("/postApplications")
	  List<PostApplication> all() {
	    return repository.findAll();
	  }
	

	  @PostMapping("/postApplications")
	  PostApplication newPostApplication(@RequestBody PostApplication newPostApplication) {
	    return repository.save(newPostApplication);
	  }
	


	  @DeleteMapping("/postApplications/{id}")
	  void deletePostApplication(@PathVariable Long id) {
	    repository.deleteById(id);
	  }
}
