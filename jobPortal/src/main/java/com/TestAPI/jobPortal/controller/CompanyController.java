package com.TestAPI.jobPortal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.TestAPI.jobPortal.exception.CompanyNotFoundException;
import com.TestAPI.jobPortal.model.Company;
import com.TestAPI.jobPortal.repository.CompanyRepository;

@RestController
public class CompanyController {

	@Autowired
	  private final CompanyRepository repository;
	  CompanyController(CompanyRepository repository) {
	    this.repository = repository;
	  }

	  @GetMapping("/companies")
	  List<Company> all() {
	    return repository.findAll();
	  }
	

	  @PostMapping("/companies")
	  Company newCompany(@RequestBody Company newCompany) {
	    return repository.save(newCompany);
	  }
	

	  @PutMapping("/companies/{id}")
	  Company replaceCompany(@RequestBody Company newCompany, @PathVariable Long id) {
	    
	    return repository.findById(id).map(company -> {company.setName(newCompany.getName());
	        return repository.save(company);}).orElseGet(() -> {
	    	  newCompany.setId(id);
	        return repository.save(newCompany);
	      });
	  }

	  @DeleteMapping("/companies/{id}")
	  void deleteCompany(@PathVariable Long id) {
	    repository.deleteById(id);
	  }
}

