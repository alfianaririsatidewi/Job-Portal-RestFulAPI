package com.TestAPI.jobPortal.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Company")
public class Company {
	
	  @Id @GeneratedValue
	  @Column(name = "COMPANY_ID",unique=true, nullable = false)
	  private Long id;
	  
	  @Column(name = "COMPANY_NAME")
	  private String name;
	  
	 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	}
