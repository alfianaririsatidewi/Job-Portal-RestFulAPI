package com.TestAPI.jobPortal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.TestAPI.jobPortal.other.Status;

@Entity
@Table(name = "PostJob")
public class PostJob {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "post_seq")
    
    @Column(name = "POST_JOB_ID",unique=true, nullable = false)
    private Long postJobId;
	
	  @ManyToOne
	  @JoinColumn(name = "JOB_ID")
	  private Job job;
	  
	  @ManyToOne
	  @JoinColumn(name = "COMPANY_ID")
	  private Company company;
	  
	    @Column(name = "status")
	    private Status status;

		public Long getPostJobId() {
			return postJobId;
		}

		public void setPostJobId(Long postJobId) {
			this.postJobId = postJobId;
		}

		public Job getJob() {
			return job;
		}

		public void setJob(Job job) {
			this.job = job;
		}

		public Company getCompany() {
			return company;
		}

		public void setCompany(Company company) {
			this.company = company;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}
	    
	    
}
