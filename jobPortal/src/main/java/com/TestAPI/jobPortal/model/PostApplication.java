package com.TestAPI.jobPortal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PostApplication")
public class PostApplication {
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "post_seq")
    
    @Column(name = "POST_ID",unique=true, nullable = false)
    private Long postApplicationId;
	

	  @ManyToOne
	  @JoinColumn(name = "FREELANCE_ID")
	  private Freelance freelance;

	  @ManyToOne
	  @JoinColumn(name = "JOB_ID")
	  private Job job;
	  
	  @ManyToOne
	  @JoinColumn(name = "COMPANY_ID")
	  private Company company;
	  
	  @Column(name = "file_cv")
	  private String fileCv;

	public Long getPostApplicationId() {
		return postApplicationId;
	}

	public void setPostApplicationId(Long postApplicationId) {
		this.postApplicationId = postApplicationId;
	}

	public Freelance getFreelance() {
		return freelance;
	}

	public void setFreelance(Freelance freelance) {
		this.freelance = freelance;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getFileCv() {
		return fileCv;
	}

	public void setFileCv(String fileCv) {
		this.fileCv = fileCv;
	}

	  
}
