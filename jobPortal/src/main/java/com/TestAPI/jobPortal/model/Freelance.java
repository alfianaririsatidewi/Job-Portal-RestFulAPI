package com.TestAPI.jobPortal.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Freelance")
public class Freelance {
		@Id @GeneratedValue
	  @Column(name = "FREELANCE_ID",unique=true, nullable = false)
	  private Long id;
	  
		@Column(name = "FREELANCE_NAME")
		  private String freelanceName;
		
		

	  public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getFreelanceName() {
			return freelanceName;
		}

		public void setFreelanceName(String freelanceName) {
			this.freelanceName = freelanceName;
		}

}
