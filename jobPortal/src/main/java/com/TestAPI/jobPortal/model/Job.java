package com.TestAPI.jobPortal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Job")
public class Job {
	@Id @GeneratedValue
	 @Column(name = "JOB_ID",unique=true, nullable = false)
	 private Long jobId;
	 @Column(name = "JOB_TITLE")
	 private String jobTitle;
	 
	public Long getJobId() {
		return jobId;
	}
	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	 
}
