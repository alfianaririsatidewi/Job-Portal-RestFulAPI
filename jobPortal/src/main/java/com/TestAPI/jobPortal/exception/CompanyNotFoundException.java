package com.TestAPI.jobPortal.exception;

public class CompanyNotFoundException extends RuntimeException {
	CompanyNotFoundException(Long id) {
	    super("Could not find Company " + id);
	  }
}
