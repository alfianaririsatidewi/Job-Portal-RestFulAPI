package com.TestAPI.jobPortal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TestAPI.jobPortal.model.Job;

public interface JobRepository extends JpaRepository<Job, Long>{

}
