package com.TestAPI.jobPortal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TestAPI.jobPortal.model.PostApplication;

public interface PostApplicationRepository extends JpaRepository<PostApplication, Long>{

}
