package com.TestAPI.jobPortal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TestAPI.jobPortal.model.Freelance;

public interface FreelanceRepository extends JpaRepository<Freelance, Long>{

}
