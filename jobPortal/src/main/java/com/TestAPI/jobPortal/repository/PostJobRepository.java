package com.TestAPI.jobPortal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TestAPI.jobPortal.model.PostJob;

public interface PostJobRepository extends JpaRepository<PostJob, Long>{

}
