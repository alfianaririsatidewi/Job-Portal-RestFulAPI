package com.TestAPI.jobPortal.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.TestAPI.jobPortal.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>{

}
