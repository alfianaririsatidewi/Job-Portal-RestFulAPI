package com.TestAPI.jobPortal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.TestAPI.jobPortal.model.Company;
import com.TestAPI.jobPortal.repository.CompanyRepository;

@Configuration
public class LoadDatabase {
	  private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

	  @Bean
	  CommandLineRunner initDatabase(CompanyRepository repository) {

	    return args -> {
	      log.info("Preloading " + repository.save(new Company("PT Sri Jaya")));
	      log.info("Preloading " + repository.save(new Company("CV mulia", "Staff HRD")));
	    };
	  }
}
